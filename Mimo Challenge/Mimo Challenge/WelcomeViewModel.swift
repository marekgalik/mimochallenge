import Foundation
import RxSwift

class WelcomeViewModel: WelcomeViewModelProtocol {
    let welcomeMessage: BehaviorSubject<String>
    let confirmText: BehaviorSubject<String>
    let startPlaying: () -> Void
    
    init(startPlaying: @autoclosure @escaping () -> Void) {
        welcomeMessage = BehaviorSubject<String>(value: "Welcome to Mimo demo app")
        confirmText = BehaviorSubject<String>(value: "Start playing")
        self.startPlaying = startPlaying
    }
}
