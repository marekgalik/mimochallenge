import Foundation

class RootViewModel: RootViewModelProtocol {
    let welcomeViewModel: WelcomeViewModelProtocol
    let gameViewModel: GameViewModelProtocol
    
    init(welcomeViewModel: WelcomeViewModelProtocol, gameViewModel: GameViewModelProtocol) {
        self.welcomeViewModel = welcomeViewModel
        self.gameViewModel = gameViewModel
    }
}
