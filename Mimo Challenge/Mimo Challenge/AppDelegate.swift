import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let rootView = RootView()
       
        let welcomeViewModel = WelcomeViewModel(startPlaying: rootView.showGameView())
        rootView.setup(with: RootViewModel(welcomeViewModel: welcomeViewModel,
                                           gameViewModel: createGameViewModel(using: rootView)))
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootView
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        saveContext()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        saveContext()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        saveContext()
    }

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Mimo_Challenge")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    private func createGameViewModel(using rootView: RootView) -> GameViewModel {
        return GameViewModel(mimoEndpoint: MimoEndpointManager(), viewContext: persistentContainer.viewContext, wrongAnswerAction: {
            rootView.presentNotCorrectAnswerAlert()
        }) { completion in
            let request = NSFetchRequest<LessonEntity>(entityName: String(describing: LessonEntity.self))
            let lessons = try? self.persistentContainer.viewContext.fetch(request)
            lessons?.forEach{
                print("Lesson with id: \($0.id) started: \($0.started.debugDescription), finished \($0.finished.debugDescription)")
            }
            
            rootView.presentAllLessonsCorrect(completion)
        }
    }

}

