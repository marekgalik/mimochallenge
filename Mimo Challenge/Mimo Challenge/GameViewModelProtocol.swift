import Foundation
import RxSwift

protocol GameViewModelProtocol {
    var lessonToPresent: Observable<Lesson?> { get }
    var wrongAnswerAction: () -> Void { get }
    var allLessonsCompleted: (@escaping () -> Void) -> Void { get }
    
    func requestLessons()
    func checkAnswer(_ answer: String, with completion: @escaping (Bool) -> Void)
}
