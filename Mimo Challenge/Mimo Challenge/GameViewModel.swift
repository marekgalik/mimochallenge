import Foundation
import RxSwift
import RxCocoa
import CoreData

class GameViewModel: GameViewModelProtocol {
    let allLessonsCompleted: (@escaping () -> Void) -> Void
    
    let wrongAnswerAction: () -> Void
    let lessonToPresent: Observable<Lesson?>
    private let viewContext: NSManagedObjectContext
    
    private let privateLessons = BehaviorRelay<(Lesson, Date)?>(value: nil)
    private var lessons = [Lesson]()
    private let mimoEndpoint: MimoEndpointProtocol
    private let disposeBag = DisposeBag()
    
    init(mimoEndpoint: MimoEndpointProtocol, viewContext: NSManagedObjectContext, wrongAnswerAction: @escaping () -> Void, allLessonsCompleted: @escaping (@escaping () -> Void) -> Void) {
        self.mimoEndpoint = mimoEndpoint
        self.wrongAnswerAction = wrongAnswerAction
        self.allLessonsCompleted = allLessonsCompleted
        self.viewContext = viewContext
        
        lessonToPresent = privateLessons.map { $0?.0 }.asObservable()
    }
    
    func requestLessons() {
        mimoEndpoint.provideChallenges { [unowned self] mimoResponse in
            guard let lessons = mimoResponse?.lessons else {
                return
            }
            self.lessons = lessons
            self.privateLessons.accept((self.lessons.removeFirst(), Date()))
        }
    }
    
    func checkAnswer(_ answer: String, with completion: @escaping (Bool) -> Void) {
        if privateLessons.value?.0.guessedText == answer {
            viewContext.performAndWait {
                let lessonEntity = LessonEntity(context: viewContext)
                lessonEntity.id = Int64(privateLessons.value?.0.id ?? 0)
                lessonEntity.started = privateLessons.value?.1 ?? Date()
                lessonEntity.finished = Date()
                
                try? viewContext.save()
                
                completion(true)
            }
            if !lessons.isEmpty {
                privateLessons.accept((lessons.removeFirst(), Date()))
            } else {
                privateLessons.accept(nil)
            }
        } else {
            completion(false)
        }
    }
}
