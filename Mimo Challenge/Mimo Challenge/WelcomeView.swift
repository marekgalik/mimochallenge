import UIKit
import RxSwift
import RxCocoa

protocol WelcomeViewModelProtocol {
    var welcomeMessage: BehaviorSubject<String> { get }
    var confirmText: BehaviorSubject<String> { get }
    var startPlaying: () -> Void { get }
}

class WelcomeView: UIView {
    private var viewModel: WelcomeViewModelProtocol?
    private var disposeBag = DisposeBag()
    private let welcomeLabel = UILabel()
    private let confirmButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    func setup(with viewModel: WelcomeViewModelProtocol) {
        disposeBag = DisposeBag()
        self.viewModel = viewModel
        
        viewModel.welcomeMessage
            .observeOn(MainScheduler.instance)
            .bind(to: welcomeLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.confirmText
            .observeOn(MainScheduler.instance)
            .bind(to: confirmButton.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
        confirmButton.rx.tap
            .subscribe { [weak self] _ in
                self?.viewModel?.startPlaying()
        }
            .disposed(by: disposeBag)
    }
    
    private func setupUI() {
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.backgroundColor = .mimoPurple
        confirmButton.layer.cornerRadius = 10.0
        
        addSubview(welcomeLabel)
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        welcomeLabel.textAlignment = .center
        
        addSubview(confirmButton)
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        
        confirmButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "V:[welcomeLabel]-(20)-[confirmButton(40)]",
                                           options: [],
                                           metrics: nil,
                                           views: ["confirmButton": confirmButton, "welcomeLabel": welcomeLabel]))
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(10.0)-[confirmButton]-(10.0)-|",
                                           options: [],
                                           metrics: nil,
                                           views: ["confirmButton": confirmButton]))
        
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(10.0)-[welcomeLabel]-(10.0)-|",
                                           options: [],
                                           metrics: nil,
                                           views: ["welcomeLabel": welcomeLabel]))
        
    }
}
