import UIKit

extension UIViewController {
    func presentNotCorrectAnswerAlert() {
        let alert = UIAlertController(title: "Wrong Answer", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func presentAllLessonsCorrect(_ completion: @escaping () -> Void) {
        let alert = UIAlertController(title: "All Lessons were completed", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Start Again!", style: .default) { _ in
            completion()
        }
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
}
