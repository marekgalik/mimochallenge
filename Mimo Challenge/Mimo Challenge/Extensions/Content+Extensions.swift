import UIKit
import Hex

extension Content {
    var uiColor: UIColor {
        return UIColor(hex: color ?? "")
    }
}
