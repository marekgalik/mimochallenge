import UIKit

extension UIColor {
    static var mimoPurple: UIColor {
        return UIColor(red: 125/255, green: 115/255, blue: 195/255, alpha: 1.0)
    }
    
    static var mimoGray: UIColor {
        return UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
    }
}
