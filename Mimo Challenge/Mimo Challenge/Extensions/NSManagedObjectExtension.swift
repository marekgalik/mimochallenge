import Foundation
import CoreData

extension NSManagedObject {
    static func newEntity(context: NSManagedObjectContext) -> Self {
        return createNewEntity(context: context)
    }
    
    private static func createNewEntity<T>(context: NSManagedObjectContext) -> T {
        let classname = String(describing: self)
        let object = NSEntityDescription.insertNewObject(forEntityName: classname, into: context) as! T
        
        return object
    }
}
