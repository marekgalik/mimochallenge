import UIKit

extension UIView {
    func coverWholeSuperview() {
        superview?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        superview?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        superview?.topAnchor.constraint(equalTo: topAnchor).isActive = true
        superview?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
