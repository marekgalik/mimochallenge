import Foundation
import Substring
import Hex

extension Lesson {
    var guessedText: String? {
        guard let content = content, let startIndex = input?.startIndex, let endIndex = input?.endIndex else { return "" }
        
        return content
            .reduce("") { result, content in
                guard let text = content.text else { return result }
                return result.appending(text)
        }[startIndex...endIndex]?.string
    }
    
    var wholeText: NSAttributedString {
        guard let content = content else { return NSAttributedString() }
        
        return content
            .reduce(NSMutableAttributedString(string: "")) { result, content in
                guard let text = content.text else { return result }
                result.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: content.uiColor]))
            
                return result
        }
    }
}
