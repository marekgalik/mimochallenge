import UIKit
import RxSwift
import RxCocoa
import Hex

class GameView: UIView {
    private var viewModel: GameViewModelProtocol?
    
    private let lessonStackView = UIStackView()
    private let lessonTextField = UITextField()
    private let confirmButton = UIButton()
    private var lessonTextFieldWidthConstraint = NSLayoutConstraint()
    private var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    func setup(with viewModel: GameViewModelProtocol) {
        disposeBag = DisposeBag()
        self.viewModel = viewModel
        
        viewModel.lessonToPresent
            .asDriver(onErrorJustReturn: nil)
            .drive(onNext: { [weak self] lesson in
                if let lesson = lesson {
                    self?.setup(with: lesson)
                } else {
                    viewModel.allLessonsCompleted {
                        viewModel.requestLessons()
                    }
                }
                
            })
            .disposed(by: disposeBag)
        
        viewModel.requestLessons()
        
        confirmButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] _ in
                viewModel.checkAnswer(self?.lessonTextField.text?.lowercased() ?? "", with: { correct in
                    if !correct {
                        viewModel.wrongAnswerAction()
                    }
                })
            })
            .disposed(by: disposeBag)
    }
    
    private func setup(with lesson: Lesson) {
        lessonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        lessonTextField.text = nil
        
        if lesson.guessedText?.isEmpty ?? true {
            lessonStackView.addArrangedSubview(label(with: lesson.wholeText))
        } else {
            let text1 = lesson.wholeText.attributedSubstring(from: NSRange(location: 0, length: lesson.input!.startIndex!))
            lessonStackView.addArrangedSubview(label(with: text1))
            lessonTextFieldWidthConstraint.constant = lessonTextField.width(for: lesson.guessedText ?? "")
            lessonStackView.addArrangedSubview(lessonTextField)
            
            let text2 = lesson.wholeText.attributedSubstring(from: NSRange(location: lesson.input!.endIndex!, length: lesson.wholeText.length - lesson.input!.endIndex!))
            lessonStackView.addArrangedSubview(label(with: text2))
        }
    }
    
    private func label(with text: NSAttributedString) -> UILabel {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.backgroundColor = .lightGray
        label.attributedText = text
        label.sizeToFit()
        
        return label
    }
    
    private func setupUI() {
        lessonStackView.axis = .horizontal
        lessonStackView.distribution = .fill
        lessonStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(lessonStackView)
        
        confirmButton.backgroundColor = UIColor.mimoPurple
        confirmButton.layer.cornerRadius = 10.0
        confirmButton.setTitle("Check Answer", for: .normal)
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(confirmButton)
        
        lessonStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        lessonStackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        lessonStackView.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        lessonTextFieldWidthConstraint = lessonTextField.widthAnchor.constraint(equalToConstant: 0.0)
        lessonTextFieldWidthConstraint.isActive = true
        lessonTextField.returnKeyType = .done
        lessonTextField.delegate = self
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(10.0)-[confirmButton]-(10.0)-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["confirmButton": confirmButton]))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:[confirmButton(40.0)]-(20.0)-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["confirmButton": confirmButton]))
    }
}

extension GameView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
