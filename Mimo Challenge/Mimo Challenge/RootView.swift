import UIKit
import RxSwift
import RxCocoa

protocol RootViewModelProtocol {
    var welcomeViewModel: WelcomeViewModelProtocol { get }
    var gameViewModel: GameViewModelProtocol { get }
}

class RootView: UIViewController {
    let welcomeView = WelcomeView()
    let gameView = GameView()
    
    var viewModel: RootViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setup(with viewModel: RootViewModelProtocol) {
        self.viewModel = viewModel
        welcomeView.setup(with: viewModel.welcomeViewModel)
        gameView.setup(with: viewModel.gameViewModel)
    }
    
    private func setupUI() {
        gameView.isHidden = true
        view.backgroundColor = .mimoGray
        
        welcomeView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(welcomeView)
        welcomeView.coverWholeSuperview()
        
        gameView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(gameView)
        gameView.coverWholeSuperview()
    }
    
    func showGameView() {
        gameView.alpha = 0.0
        gameView.isHidden = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.gameView.alpha = 1.0
            self.welcomeView.alpha = 0.0
        }) { successfuly in
            if successfuly {
                self.welcomeView.isHidden = true
                self.welcomeView.alpha = 0.0
            }
        }
    }
    
}



