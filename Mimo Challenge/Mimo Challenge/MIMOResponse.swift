import Foundation

struct MIMOResponse: Codable {
    let lessons: [Lesson]?
    
    enum CodingKeys: String, CodingKey {
        case lessons = "lessons"
    }
}

struct Lesson: Codable {
    let id: Int?
    let content: [Content]?
    let input: Input?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case content = "content"
        case input = "input"
    }
}

struct Content: Codable {
    let color: String?
    let text: String?
    
    enum CodingKeys: String, CodingKey {
        case color = "color"
        case text = "text"
    }
}

struct Input: Codable {
    let startIndex: Int?
    let endIndex: Int?
    
    enum CodingKeys: String, CodingKey {
        case startIndex = "startIndex"
        case endIndex = "endIndex"
    }
}

// MARK: Convenience initializers and mutators

extension MIMOResponse {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(MIMOResponse.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        lessons: [Lesson]?? = nil
        ) -> MIMOResponse {
        return MIMOResponse(
            lessons: lessons ?? self.lessons
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Lesson {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Lesson.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        content: [Content]?? = nil,
        input: Input?? = nil
        ) -> Lesson {
        return Lesson(
            id: id ?? self.id,
            content: content ?? self.content,
            input: input ?? self.input
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Content {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Content.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        color: String?? = nil,
        text: String?? = nil
        ) -> Content {
        return Content(
            color: color ?? self.color,
            text: text ?? self.text
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Input {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Input.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        startIndex: Int?? = nil,
        endIndex: Int?? = nil
        ) -> Input {
        return Input(
            startIndex: startIndex ?? self.startIndex,
            endIndex: endIndex ?? self.endIndex
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
