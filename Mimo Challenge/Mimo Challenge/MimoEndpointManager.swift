import Foundation

protocol MimoEndpointProtocol {
    func provideChallenges(_ completion: @escaping (MIMOResponse?) -> Void)
}

class MimoEndpointManager: MimoEndpointProtocol {
    private let url: URL
    
    init(with url: URL = URL(string: "https://mimochallenge.azurewebsites.net/api/lessons")!) {
        self.url = url
    }
    
    func provideChallenges(_ completion: @escaping (MIMOResponse?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, !data.isEmpty, let response = try? MIMOResponse(data: data) else {
                completion(nil)
                return
            }
            
            completion(response)
        }.resume()
        
    }
}
